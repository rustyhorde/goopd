use {GoopdResult, STDERR_SW, STDOUT_SW};
use connection::GoopdConn;
use errors::GoopdErr;
use mio::{EventLoop, EventSet, Handler, PollOpt, Token};
use mio::tcp::TcpListener;
use slab::Slab;
use slog::Logger;
use slog::drain::IntoLogger;

/// The goop daemon (goopd) eventloop struct.
pub struct Goopd {
    sock: TcpListener,
    token: Token,
    conns: Slab<GoopdConn, Token>,
    stdout: Logger,
    stderr: Logger,
}

impl Goopd {
    /// Create a new goopd.
    pub fn new(srv: TcpListener) -> Goopd {
        Goopd {
            sock: srv,
            token: Token(1),
            conns: Slab::new_starting_at(Token(2), 128),
            stdout: STDOUT_SW.drain().into_logger(o!("struct" => "Goopd")),
            stderr: STDERR_SW.drain().into_logger(o!("struct" => "Goopd")),
        }
    }

    /// Accept a _new_ client connection.
    ///
    /// The server will keep track of the new connection and forward any events from the event loop
    /// to this connection.
    fn accept(&mut self, event_loop: &mut EventLoop<Goopd>) {
        trace!(self.stdout, "accept", "message" => "accepting new connection");

        // Log an error if there is no socket, but otherwise move on so we do not tear down the
        // entire server.
        let (sock, addr) = match self.sock.accept() {
            Ok(s) => {
                if let Some(sock) = s {
                    sock
                } else {
                    error!(
                        self.stderr,
                        "accept",
                        "error" => "Failed to accept new connection",
                        "detail" => "Bad socket"
                    );
                    self.reregister(event_loop);
                    return;
                }
            }
            Err(e) => {
                error!(
                    self.stderr,
                    "accept",
                    "error" => "Failed to accept new connection",
                    "detail" => format!("{}", e)
                );
                self.reregister(event_loop);
                return;
            }
        };

        let token = if let Some(vacant) = self.conns.vacant_entry() {
            let token = vacant.index();
            if let Ok(new_conn) = GoopdConn::new(sock, token) {
                if let Ok(_) = event_loop.register(new_conn.sock(),
                                                   token,
                                                   EventSet::readable(),
                                                   PollOpt::edge() | PollOpt::oneshot()) {
                    trace!(
                        self.stdout,
                        "accept",
                        "token" => token.0,
                        "addr" => format!("{}", addr)
                    );
                    vacant.insert(new_conn);
                    Some(token)
                } else {
                    None
                }
            } else {
                None
            }
        } else {
            None
        };


        if let Some(tok) = token {
            let ves_stdout = self.stdout.new(o!("ves" => true));
            let ves_stderr = self.stderr.new(o!("ves" => true));
            if let Ok(conn) = self.get_connection(tok) {
                let id_bytes = conn.id().to_string().into_bytes();
                if let Ok(count) = conn.write(event_loop, &id_bytes) {
                    trace!(ves_stdout, "accept", "bytes_written" => count);
                }
            } else {
                let msg = format!("Unable to get connection for Token({})", tok.0);
                error!(ves_stderr, "accept", "error" => msg);
            }
        } else {
            error!(
                self.stderr,
                "accept",
                "error" => "Failed to accept new connection",
                "detail" => "Bad Token"
            );
            self.reregister(event_loop);
            return;
        }

        // We are using edge-triggered polling. Even our `Goopd` token needs to reregister.
        self.reregister(event_loop);
    }

    /// Get the connection for the given token and call readable on that connection.
    fn conn_readable(&mut self, event_loop: &mut EventLoop<Goopd>, tok: Token) -> GoopdResult<()> {
        self.get_connection(tok).and_then(|conn| conn.readable(event_loop))
    }

    /// Get the connection for the given token and call writable on that connection.
    fn conn_writable(&mut self, event_loop: &mut EventLoop<Goopd>, tok: Token) -> GoopdResult<()> {
        self.get_connection(tok).and_then(|conn| conn.writable(event_loop))
    }

    /// Find a connection in the slab using the given token.
    fn get_connection(&mut self, token: Token) -> GoopdResult<&mut GoopdConn> {
        match self.conns.get_mut(token) {
            Some(conn) => Ok(conn),
            None => Err(GoopdErr::InvalidConnection),
        }
    }

    /// Register ourselves with the event loop
    pub fn register(&mut self, event_loop: &mut EventLoop<Goopd>) {
        if let Err(e) = event_loop.register(&self.sock,
                                            self.token,
                                            EventSet::readable(),
                                            PollOpt::edge() | PollOpt::oneshot()) {
            error!(
                self.stderr,
                "register",
                "error" => "Failed to register daemon token",
                "token" => self.token.0,
                "detail" => format!("{}", e)
            );
            let server_token = self.token;
            self.reset_connection(event_loop, server_token);
        }
    }

    /// Re-register ourselves with the event loop.
    fn reregister(&mut self, event_loop: &mut EventLoop<Goopd>) {
        if let Err(e) = event_loop.reregister(&self.sock,
                                              self.token,
                                              EventSet::readable(),
                                              PollOpt::edge() | PollOpt::oneshot()) {
            error!(
                self.stderr,
                "reregister",
                "error" => "Failed to reregister daemon token",
                "token" => self.token.0,
                "detail" => format!("{}", e)
            );
            let server_token = self.token;
            self.reset_connection(event_loop, server_token);
        }
    }

    fn reset_connection(&mut self, event_loop: &mut EventLoop<Goopd>, token: Token) {
        if self.token == token {
            event_loop.shutdown();
        } else {
            self.conns.remove(token);
        }
    }
}

impl Handler for Goopd {
    type Timeout = ();
    type Message = ();

    fn ready(&mut self, event_loop: &mut EventLoop<Goopd>, token: Token, events: EventSet) {
        if token == Token(0) {
            error!(self.stderr, "ready", "error" => "[BUG]: Received event for Token(0)");
        } else {
            if events.is_hup() {
                trace!(self.stdout, "ready", "event" => "hup", "token" => token.0);
                self.reset_connection(event_loop, token);
                return;
            }

            if events.is_error() {
                error!(self.stderr, "ready", "event" => "error", "token" => token.0);
                self.reset_connection(event_loop, token);
                return;
            }

            if events.is_readable() {
                // A read event for our `Goop` token means we are establishing a new connection. A
                // read event for any other token should be handed off to that connection.
                if self.token == token {
                    trace!(self.stdout, "ready", "event" => "readable", "token" => token.0);
                    self.accept(event_loop);
                } else if let Err(e) = self.conn_readable(event_loop, token) {
                    error!(
                        self.stderr,
                        "ready",
                        "event" => "readable",
                        "error" => "conn_readable failed",
                        "token" => token.0,
                        "detail" => format!("{}", e)
                    );
                    self.reset_connection(event_loop, token);
                }
            }

            if events.is_writable() {
                // We never expect a write event for our `Goop` token . A write event for any other
                // token should be handed off to that connection.
                if self.token == token {
                    error!(
                        self.stderr,
                        "ready",
                        "event" => "readable",
                        "token" => token.0,
                        "error" => "write event on daemon token"
                    );
                } else if let Err(e) = self.conn_writable(event_loop, token) {
                    error!(
                        self.stderr,
                        "ready",
                        "event" => "writable",
                        "error" => "conn_writable failed",
                        "token" => token.0,
                        "detail" => format!("{}", e)
                    );
                    self.reset_connection(event_loop, token);
                }
            }
        }
    }
}
