use goopy::error::GoopyErr;
use std::error::Error;
use std::{ffi, fmt, io, net, num};

#[derive(Debug)]
pub enum GoopdErr {
    Config,
    Goopy(GoopyErr),
    Io(io::Error),
    InvalidConnection,
    InvalidKexPayload,
    InvalidReadableState,
    InvalidVersionExchange,
    Nul(ffi::NulError),
    ParseAddr(net::AddrParseError),
    ParseInt(num::ParseIntError),
}

impl fmt::Display for GoopdErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            GoopdErr::Config => write!(f, "Error parsing config toml"),
            GoopdErr::Goopy(ref e) => write!(f, "{}", e),
            GoopdErr::Io(ref e) => write!(f, "{}", e),
            GoopdErr::InvalidConnection => write!(f, "Invalid connection"),
            GoopdErr::InvalidKexPayload => write!(f, "Invalid key exchange payload"),
            GoopdErr::InvalidReadableState => write!(f, "Invalid readable state"),
            GoopdErr::InvalidVersionExchange => write!(f, "Invalid version exchanges"),
            GoopdErr::Nul(ref e) => write!(f, "{}", e),
            GoopdErr::ParseAddr(ref e) => write!(f, "{}", e),
            GoopdErr::ParseInt(ref e) => write!(f, "{}", e),
        }
    }
}

impl Error for GoopdErr {
    fn description(&self) -> &str {
        match *self {
            GoopdErr::Config => "Error parsing config toml",
            GoopdErr::Goopy(ref e) => e.description(),
            GoopdErr::Io(ref e) => e.description(),
            GoopdErr::InvalidConnection => "Invalid connection",
            GoopdErr::InvalidKexPayload => "Invalid key exchange payload",
            GoopdErr::InvalidReadableState => "Invalid readable state",
            GoopdErr::InvalidVersionExchange => "Invalid version exchange",
            GoopdErr::Nul(ref e) => e.description(),
            GoopdErr::ParseAddr(ref e) => e.description(),
            GoopdErr::ParseInt(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            GoopdErr::Config |
            GoopdErr::InvalidConnection |
            GoopdErr::InvalidKexPayload |
            GoopdErr::InvalidReadableState |
            GoopdErr::InvalidVersionExchange => None,
            GoopdErr::Goopy(ref e) => Some(e),
            GoopdErr::Io(ref e) => Some(e),
            GoopdErr::Nul(ref e) => Some(e),
            GoopdErr::ParseAddr(ref e) => Some(e),
            GoopdErr::ParseInt(ref e) => Some(e),
        }
    }
}

impl From<io::Error> for GoopdErr {
    fn from(e: io::Error) -> GoopdErr {
        GoopdErr::Io(e)
    }
}

impl From<net::AddrParseError> for GoopdErr {
    fn from(e: net::AddrParseError) -> GoopdErr {
        GoopdErr::ParseAddr(e)
    }
}

impl From<num::ParseIntError> for GoopdErr {
    fn from(e: num::ParseIntError) -> GoopdErr {
        GoopdErr::ParseInt(e)
    }
}

impl From<ffi::NulError> for GoopdErr {
    fn from(e: ffi::NulError) -> GoopdErr {
        GoopdErr::Nul(e)
    }
}

impl From<GoopyErr> for GoopdErr {
    fn from(e: GoopyErr) -> GoopdErr {
        GoopdErr::Goopy(e)
    }
}
