use clap::ArgMatches;
use std::default::Default;
use std::env;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use toml;

const CONFIG_FILE_NAME: &'static str = "goopd.toml";
const DOT_DIR: &'static str = ".goopd";

#[derive(Debug, Default, RustcDecodable)]
pub struct GoopdToml {
    ipv4: Option<Ipv4ConnectionToml>,
    ipv6: Option<Ipv6ConnectionToml>,
}

#[derive(Debug, Default, RustcDecodable)]
pub struct Ipv4ConnectionToml {
    address: Option<String>,
    port: Option<u16>,
}

#[derive(Debug, Default, RustcDecodable)]
pub struct Ipv6ConnectionToml {
    address: Option<String>,
    port: Option<u16>,
    scope_id: Option<u32>,
    flow_id: Option<u32>,
}

impl GoopdToml {
    pub fn new(matches: ArgMatches) -> GoopdToml {
        let mut toml: GoopdToml = Default::default();
        let mut ipv4_conn_toml: Ipv4ConnectionToml = Default::default();
        let mut ipv6_conn_toml: Ipv6ConnectionToml = Default::default();

        for path in &paths(matches.value_of("config")) {
            if let Ok(mut config_file) = File::open(path) {
                let mut toml_buf = vec![];
                if let Ok(_) = config_file.read_to_end(&mut toml_buf) {
                    let toml_str = String::from_utf8_lossy(&toml_buf);
                    if let Some(parsed) = toml::decode_str(&toml_str) {
                        toml = parsed;
                        break;
                    }
                }
            }
        }

        // Copy the TOML properties over.
        if let Some(ref conn) = toml.ipv4 {
            ipv4_conn_toml.set_address(conn.address.clone());
            ipv4_conn_toml.set_port(conn.port);
        }

        if let Some(ref conn) = toml.ipv6 {
            ipv6_conn_toml.set_address(conn.address.clone());
            ipv6_conn_toml.set_port(conn.port);
            ipv6_conn_toml.set_scope_id(conn.scope_id);
            ipv6_conn_toml.set_flow_id(conn.flow_id);
        }

        // Override them with command-line args if neccessary.
        if let Some(address) = matches.value_of("address") {
            ipv4_conn_toml.set_address(Some(address.to_string()));
        } else if let Some(address) = matches.value_of("address_v4") {
            ipv4_conn_toml.set_address(Some(address.to_string()));
        }

        if let Some(port) = matches.value_of("port") {
            if let Ok(port_val) = port.parse() {
                ipv4_conn_toml.set_port(Some(port_val));
            }
        } else if let Some(port) = matches.value_of("port_v4") {
            if let Ok(port_val) = port.parse() {
                ipv4_conn_toml.set_port(Some(port_val));
            }
        }

        if let Some(address) = matches.value_of("address_v6") {
            ipv6_conn_toml.set_address(Some(address.to_string()));
        }

        if let Some(port) = matches.value_of("port_v6") {
            if let Ok(port_val) = port.parse() {
                ipv6_conn_toml.set_port(Some(port_val));
            }
        }

        if let Some(scope_id) = matches.value_of("scope_id_v6") {
            if let Ok(scope_id_val) = scope_id.parse() {
                ipv6_conn_toml.set_scope_id(Some(scope_id_val));
            }
        }

        if let Some(flow_id) = matches.value_of("flow_id_v6") {
            if let Ok(flow_id_val) = flow_id.parse() {
                ipv6_conn_toml.set_flow_id(Some(flow_id_val));
            }
        }

        // Reset the connection config if neccessary
        if ipv4_conn_toml.address.is_some() || ipv4_conn_toml.port.is_some() {
            toml.ipv4 = Some(ipv4_conn_toml);
        }

        if ipv6_conn_toml.address.is_some() || ipv6_conn_toml.port.is_some() ||
           ipv6_conn_toml.scope_id.is_some() || ipv6_conn_toml.flow_id.is_some() {
            toml.ipv6 = Some(ipv6_conn_toml);
        }
        toml
    }

    pub fn ipv4(&self) -> Option<&Ipv4ConnectionToml> {
        match self.ipv4 {
            Some(ref c) => Some(c),
            None => None,
        }
    }

    pub fn ipv6(&self) -> Option<&Ipv6ConnectionToml> {
        match self.ipv6 {
            Some(ref c) => Some(c),
            None => None,
        }
    }
}

impl Ipv4ConnectionToml {
    pub fn address(&self) -> Option<&String> {
        match self.address {
            Some(ref s) => Some(s),
            None => None,
        }
    }

    pub fn port(&self) -> Option<u16> {
        self.port
    }

    pub fn set_address(&mut self, address: Option<String>) -> &mut Ipv4ConnectionToml {
        self.address = address;
        self
    }

    pub fn set_port(&mut self, port: Option<u16>) -> &mut Ipv4ConnectionToml {
        self.port = port;
        self
    }
}

impl Ipv6ConnectionToml {
    pub fn address(&self) -> Option<&String> {
        match self.address {
            Some(ref s) => Some(s),
            None => None,
        }
    }

    pub fn port(&self) -> Option<u16> {
        self.port
    }

    pub fn scope_id(&self) -> Option<u32> {
        self.scope_id
    }

    pub fn flow_id(&self) -> Option<u32> {
        self.flow_id
    }

    pub fn set_address(&mut self, address: Option<String>) -> &mut Ipv6ConnectionToml {
        self.address = address;
        self
    }

    pub fn set_port(&mut self, port: Option<u16>) -> &mut Ipv6ConnectionToml {
        self.port = port;
        self
    }

    pub fn set_scope_id(&mut self, scope_id: Option<u32>) -> &mut Ipv6ConnectionToml {
        self.scope_id = scope_id;
        self
    }

    pub fn set_flow_id(&mut self, flow_id: Option<u32>) -> &mut Ipv6ConnectionToml {
        self.flow_id = flow_id;
        self
    }
}

fn paths(arg: Option<&str>) -> Vec<PathBuf> {
    let mut paths = Vec::new();

    if let Some(dir) = arg {
        paths.push(PathBuf::from(dir));
    }

    if let Ok(mut cur_dir) = env::current_dir() {
        cur_dir.push(DOT_DIR);
        cur_dir.push(CONFIG_FILE_NAME);
        paths.push(cur_dir);
    }

    if let Some(mut home_dir) = env::home_dir() {
        home_dir.push(DOT_DIR);
        home_dir.push(CONFIG_FILE_NAME);
        paths.push(home_dir);
    }

    add_system_path(&mut paths);
    paths
}

#[cfg(windows)]
fn add_system_path(paths: &mut Vec<PathBuf>) {
    if let Ok(appdata) = env::var("APPDATA") {
        let mut appdata_path = PathBuf::from(appdata);
        appdata_path.push(DOT_DIR);
        appdata_path.push(CONFIG_FILE_NAME);
        paths.push(appdata_path);
    }
}

#[cfg(unix)]
fn add_system_path(paths: &mut Vec<PathBuf>) {
    let mut appdata = PathBuf::from("/etc");
    appdata.push("goopd");
    appdata.push(CONFIG_FILE_NAME);
    paths.push(appdata);
}
