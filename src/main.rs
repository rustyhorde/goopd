// Copyright (c) 2016 goopd developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! goopd - SSH Server Daemon
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]
#![deny(missing_docs)]
#[macro_use]
extern crate clap;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate slog;

extern crate bytes;
extern crate env_logger;
extern crate goopy;
extern crate libc;
extern crate mio;
extern crate net2;
extern crate slab;
extern crate slog_json;
extern crate slog_term;
extern crate rustc_serialize;
extern crate toml;

mod config;
mod connection;
mod daemon;
mod errors;
mod state;

use clap::{App, Arg};
use config::GoopdToml;
use errors::GoopdErr;
use mio::EventLoop;
use mio::tcp::TcpListener;
use net2::TcpBuilder;
use slog::Level;
use slog::drain::{self, AtomicSwitchCtrl, IntoLogger};
use std::fs::OpenOptions;
use std::io;
use std::net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6};
use std::process;
use std::sync::mpsc::channel;
use std::thread;

/// Goopd Version
pub const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
/// Goopd Package Name
pub const PKG: Option<&'static str> = option_env!("CARGO_PKG_NAME");

lazy_static! {
    /// stdout Drain switch
    pub static ref STDOUT_SW: AtomicSwitchCtrl = AtomicSwitchCtrl::new(
        drain::filter_level(
            Level::Error,
            drain::async_stream(io::stdout(), slog_term::format_colored())
        )
    );
    /// stderr Drain switch
    pub static ref STDERR_SW: AtomicSwitchCtrl = AtomicSwitchCtrl::new(
        drain::async_stream(io::stderr(), slog_term::format_colored())
    );
}

/// Result used in goopd.
pub type GoopdResult<T> = Result<T, GoopdErr>;

fn event_loops(config: GoopdToml) -> GoopdResult<()> {
    let stdout = STDOUT_SW.drain().into_logger(o!());
    let stderr = STDERR_SW.drain().into_logger(o!());
    let ipv4: Ipv4Addr;
    let port: u16;
    if let Some(conn_cfg) = config.ipv4() {
        ipv4 = try!(conn_cfg.address().unwrap_or(&"0.0.0.0".to_string()).parse());
        port = conn_cfg.port().unwrap_or(2222);
    } else {
        ipv4 = try!("0.0.0.0".parse());
        port = 2222
    }

    info!(stdout, "event_loop", "ipv4" => format!("{}", ipv4), "port" => port);
    let mut event_loop = try!(EventLoop::new());
    let addr = SocketAddr::V4(SocketAddrV4::new(ipv4, port));
    let tcp = try!(TcpBuilder::new_v4());
    let bind = try!(tcp.bind(&addr));
    let listener = try!(bind.listen(1024));
    let mio_listener = try!(TcpListener::from_listener(listener, &addr));
    let mut daemon = daemon::Goopd::new(mio_listener);
    daemon.register(&mut event_loop);

    let ipv6: Ipv6Addr;
    let portv6: u16;
    let scope_id: u32;
    let flow_id: u32;

    if let Some(conn_cfg) = config.ipv6() {
        ipv6 = try!(conn_cfg.address().unwrap_or(&"::".to_string()).parse());
        portv6 = conn_cfg.port().unwrap_or(2222);
        scope_id = conn_cfg.scope_id().unwrap_or(32);
        flow_id = conn_cfg.flow_id().unwrap_or(0);
    } else {
        ipv6 = try!("::".parse());
        portv6 = 2222;
        scope_id = 32;
        flow_id = 0;
    }

    info!(
        stdout,
        "event_loop",
        "ipv6" => format!("{}", ipv6),
        "port" => portv6,
        "scope_id" => scope_id,
        "flow_id" => flow_id
    );
    let mut event_loop_v6 = try!(EventLoop::new());
    let addr_v6 = SocketAddr::V6(SocketAddrV6::new(ipv6, portv6, flow_id, scope_id));
    let tcp_v6 = try!(TcpBuilder::new_v6());
    try!(tcp_v6.only_v6(true));
    let bind_v6 = try!(tcp_v6.bind(&addr_v6));
    let listener_v6 = try!(bind_v6.listen(1024));
    let mio_listener_v6 = try!(TcpListener::from_listener(listener_v6, &addr_v6));
    let mut daemon_v6 = daemon::Goopd::new(mio_listener_v6);
    daemon_v6.register(&mut event_loop_v6);

    let (tx4, rx4) = channel();
    let ipv4_stderr = stderr.clone();
    thread::spawn(move || {
        match event_loop.run(&mut daemon) {
            Ok(_) => {}
            Err(e) => {
                error!(
                    ipv4_stderr,
                    "event_loop",
                    "error" => "error running IPV4 event_loop",
                    "detail" => format!("{}", e)
                );
            }
        }

        tx4.send(0).expect("Unable to send to channel");
    });

    let (tx6, rx6) = channel();
    let ipv6_stderr = stderr.clone();
    thread::spawn(move || {
        match event_loop_v6.run(&mut daemon_v6) {
            Ok(_) => {}
            Err(e) => {
                error!(
                    ipv6_stderr,
                    "event_loop",
                    "error" => "error running IPV6 event_loop",
                    "detail" => format!("{}", e)
                );
            }
        }

        tx6.send(0).expect("Unable to send to channel");
    });

    rx4.recv().expect("Unable to receive on channel");
    rx6.recv().expect("Unable to receive on channel");
    Ok(())
}

fn run(opt_args: Option<Vec<&str>>) -> i32 {
    let app = App::new("goopd")
        .version(crate_version!())
        .author("Jason Ozias <jason.g.ozias@gmail.com>")
        .about("goopd ssh daemon")
        .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .value_name("CONFIG")
            .help("Specify a non-standard path for the config file.")
            .takes_value(true))
        .arg(Arg::with_name("address")
            .short("a")
            .long("addr")
            .value_name("IP_ADDR_V4")
            .help("Specify an IPV4 address for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .value_name("IP_PORT_V4")
            .help("Specify a IPV4 port for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("address_v4")
            .long("addr_v4")
            .conflicts_with("address")
            .value_name("IP_ADDR_V4")
            .help("Specify an IPV4 address for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("port_v4")
            .long("port_v4")
            .conflicts_with("port")
            .value_name("IP_PORT_V4")
            .help("Specify a IPV4 port for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("address_v6")
            .long("addr_v6")
            .value_name("IP_ADDR_V6")
            .help("Specify an IPV6 address for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("port_v6")
            .long("port_v6")
            .value_name("IP_PORT_V6")
            .help("Specify a IPV6 port for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("flow_id_v6")
            .long("flow_id_v6")
            .value_name("IP_FLOW_ID_V6")
            .help("Specify a IPV6 flow id for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("scope_id_v6")
            .long("scope_id_v6")
            .value_name("IP_SCOPE_ID_V6")
            .help("Specify a IPV6 scope id for the daemon to listen on.")
            .takes_value(true))
        .arg(Arg::with_name("dry_run")
            .long("dryrun")
            .help("Parse config and setup daemon, but don't run it."))
        .arg(Arg::with_name("verbose")
            .short("v")
            .multiple(true)
            .help("Set the output verbosity level (more v's = more verbose)"))
        .arg(Arg::with_name("json")
            .short("j")
            .long("json")
            .help("Enable json logging at the given path")
            .value_name("PATH")
            .takes_value(true));

    let matches = if let Some(args) = opt_args {
        app.get_matches_from(args)
    } else {
        app.get_matches()
    };

    // Setup the logging
    let level = match matches.occurrences_of("verbose") {
        0 => Level::Error,
        1 => Level::Warning,
        2 => Level::Info,
        3 => Level::Debug,
        4 | _ => Level::Trace,
    };

    let mut json_drain = None;
    if let Some(json_path) = matches.value_of("json") {
        if let Ok(json_file) = OpenOptions::new().create(true).append(true).open(json_path) {
            json_drain = Some(drain::stream(json_file, slog_json::new()));
        }
    }

    let stdout_base = drain::async_stream(io::stdout(), slog_term::format_colored());
    if let Some(json) = json_drain {
        STDOUT_SW.set(drain::filter_level(level, drain::duplicate(stdout_base, json)));
    } else {
        STDOUT_SW.set(drain::filter_level(level, stdout_base));
    }

    if matches.is_present("dry_run") {
        let stdout = STDOUT_SW.drain().into_logger(o!());
        warn!(stdout, "run", "message" => "Not starting event loop!", "dryrun" => "true");
        0
    } else if let Err(e) = event_loops(GoopdToml::new(matches)) {
        let stderr = STDERR_SW.drain().into_logger(o!());
        error!(stderr, "run", "error" => "error running event_loops", "detail" => format!("{}", e));
        1
    } else {
        0
    }
}

fn main() {
    process::exit(run(None));
}

#[cfg(test)]
mod main_test {

    use super::run;

    #[test]
    fn command_line() {
        assert!(0 == run(Some(vec!["goopd", "-vvvv", "--dryrun"])));
        assert!(0 == run(Some(vec!["goopd", "--dryrun", "-c", "test_cfg/goopd.toml"])))
    }
}
