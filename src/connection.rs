use {GoopdResult, PKG, STDERR_SW, STDOUT_SW, VERSION};
use bytes::{Buf, MutBuf, RingBuf};
use daemon::Goopd;
use errors::GoopdErr;
use goopy::message::{Payload, SshPayload, kexinit};
use goopy::packet;
use goopy::types::{SshId, VER_EXCH_PROTO, VersionExchange};
use goopy::utils::buf::{TryRead, TryWrite};
use mio::{EventLoop, EventSet, PollOpt, Token};
use mio::tcp::TcpStream;
use slog::Logger;
use slog::drain::IntoLogger;
use state;
use state::ConnectionEvent::{ClientKeyExchangeReceived, ClientVersionExchangeReceived};

pub struct GoopdConn {
    id: SshId,
    rx: RingBuf,
    tx: RingBuf,
    interest: EventSet,
    sock: TcpStream,
    token: Token,
    state: state::Connection,
    stdout: Logger,
    stderr: Logger,
    version_exchange: VersionExchange,
}

impl GoopdConn {
    pub fn new(sock: TcpStream, token: Token) -> GoopdResult<GoopdConn> {
        if let Ok(id) = SshId::new(PKG.unwrap_or("goopd"), VERSION.unwrap_or("unk"), None) {
            let mut version_exchange: VersionExchange = Default::default();
            version_exchange.set_server_version(id.id().to_string());
            Ok(GoopdConn {
                id: id,
                interest: EventSet::hup(),
                rx: RingBuf::new(2048),
                sock: sock,
                token: token,
                tx: RingBuf::new(2048),
                state: state::Connection::VersionExchange,
                stdout: STDOUT_SW.drain().into_logger(o!("struct" => "GoopdConn")),
                stderr: STDERR_SW.drain().into_logger(o!("struct" => "GoopdConn")),
                version_exchange: version_exchange,
            })
        } else {
            Err(GoopdErr::Config)
        }
    }

    pub fn id(&self) -> &str {
        self.id.id()
    }

    pub fn readable(&mut self, event_loop: &mut EventLoop<Goopd>) -> GoopdResult<()> {
        match self.sock.try_read_buf(&mut self.rx) {
            Ok(None) => {}
            Ok(Some(r)) => {
                trace!(self.stdout, "readable", "bytes_read" => r);
                let mut bytes_read = Vec::with_capacity(r);
                bytes_read.extend_from_slice(self.rx.bytes());
                match self.state {
                    state::Connection::VersionExchange => {
                        try!(self.version_exchange(&bytes_read, r));
                        let kex_init: kexinit::Message = Default::default();
                        let payload = try!(kex_init.into_bytes());
                        let packet = try!(packet::as_bytes(payload, None));
                        try!(self.write(event_loop, &packet));
                    }
                    state::Connection::KeyExchangeInit => {
                        let payload = try!(packet::into_payload(bytes_read, None));

                        if let SshPayload::KeyExchangeInit(kex) = payload {
                            debug!(self.stdout, "readable", "kex" => format!("{}", kex));
                            self.state = self.state.next(ClientKeyExchangeReceived);
                        } else {
                            return Err(GoopdErr::InvalidKexPayload);
                        }
                    }
                    _ => {
                        error!(
                            self.stderr,
                            "readable",
                            "error" => "Invalid Readable State",
                            "state" => format!("{}", self.state)
                        );
                        return Err(GoopdErr::InvalidReadableState);
                    }
                }

                Buf::advance(&mut self.rx, r);
                self.interest.insert(EventSet::readable());
            }
            Err(e) => {
                error!(self.stderr,
                    "readable",
                    "error" => "buf read error",
                    "detail" => format!("{}", e)
                );
                self.interest.remove(EventSet::readable());
            }
        };

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    pub fn sock(&self) -> &TcpStream {
        &self.sock
    }

    pub fn writable(&mut self, event_loop: &mut EventLoop<Goopd>) -> GoopdResult<()> {
        match self.sock.try_write_buf(&mut self.tx) {
            Ok(None) => {
                trace!(self.stdout, "writable", "message" => "client flushing buf; WOULDBLOCK");
                self.interest.insert(EventSet::writable());
            }
            Ok(Some(r)) => {
                trace!(self.stdout, "writable", "bytes_written" => r);
                self.interest.insert(EventSet::readable());
                self.interest.remove(EventSet::writable());
            }
            Err(e) => {
                error!(
                    self.stderr,
                    "writable",
                    "error" => "buf write error",
                    "detail" => format!("{}", e)
                );
            }
        }

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    pub fn write(&mut self, event_loop: &mut EventLoop<Goopd>, bytes: &[u8]) -> GoopdResult<usize> {
        let count = self.tx.write_slice(bytes);
        self.interest.insert(EventSet::writable());
        try!(event_loop.reregister(&self.sock,
                                   self.token,
                                   self.interest,
                                   PollOpt::edge() | PollOpt::oneshot()));
        Ok(count)
    }

    fn version_exchange(&mut self, bytes: &[u8], r: usize) -> GoopdResult<()> {
        // Parse any message here as a protocol version exchange message.  See
        // https://tools.ietf.org/html/rfc4253#section-4.2.
        // If the message is longer that 255 bytes or fails to match the message
        // format shutdown the connection
        let vep = String::from_utf8_lossy(bytes);
        if r < 256 && VER_EXCH_PROTO.is_match(&vep) {
            self.version_exchange.set_client_version(vep.into_owned());
            self.state = self.state.next(ClientVersionExchangeReceived);
            info!(
                self.stdout,
                "version_exchange",
                "version_exchange" => format!("{}", self.version_exchange)
            );
            Ok(())
        } else {
            error!(
                self.stderr,
                "version_exchange",
                "error" => "Invalid Version Exchange",
                "version_exchange" => format!("{}", vep)
            );
            Err(GoopdErr::InvalidVersionExchange)
        }
    }
}
